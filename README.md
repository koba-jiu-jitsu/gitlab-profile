# Koba Jiujitsu

![Koba Jiujitsu logo](/assets/images/logo.webp)

Koba Jiujitsu are a brazilian jiu-jitsu (BJJ) club based in Dorking, Surrey, UK.

This group houses repos for the club website which can be viewed at https://kobabjj.com, as well as other projects.
